// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'DashBoard.dart';
import 'RegistrationPage.dart';
import 'TextField.dart';
import 'button.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        //Set Up scaffold appearance
        backgroundColor: Colors.grey[400],
        body: Container(
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('assets/Zodiac/try1.gif'),
                  fit: BoxFit.cover)),
          child: SafeArea(
              child: Center(
                  child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              //Welcome text

              Text(
                'Dear Pisces',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 34,
                  color: Colors.white,
                ),
              ),
              SizedBox(height: 10),
              Text(
                'Welcome!',
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 24,
                    color: Colors.deepOrangeAccent,
                    fontFamily: 'IndieFlower'),
              ),

              //Space Betweeen
              SizedBox(height: 10),

              //Good Feel Text
              Text(
                'Happy to see you!!',
                style: TextStyle(
                  fontSize: 20,
                  color: Colors.white,
                ),
              ),

              //Space Between
              SizedBox(height: 30),

              //Email TextField
              TextFClass(obscureText: false, textDescript: "Email Address"),

              //Space Between
              SizedBox(height: 30),

              //Password TextField
              TextFClass(
                obscureText: false,
                textDescript: 'Password',
              ),

              //Space between
              SizedBox(height: 30),

              //Signin Button
              button(
                name: 'Sign In',
                route: DashBoard(),
                colorful: Colors.deepOrangeAccent,
              ),
              //Space between
              SizedBox(height: 16),

              //not a member? Register
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'Not a member ?',
                    style: TextStyle(color: Colors.white, fontSize: 17.0),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  button(
                    colorful: Colors.blueGrey,
                    name: 'Register Here',
                    route: Registration(),
                  ),
                ],
              ),
            ],
          ))),
        ));
  }
}

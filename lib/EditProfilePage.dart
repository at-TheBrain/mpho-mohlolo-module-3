import 'package:flutter/material.dart';
import 'package:universe/DashBoard.dart';
import 'TextField.dart';
import 'button.dart';

class Profile extends StatelessWidget {
  const Profile({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Edit Profile'),
          elevation: 15,
          backgroundColor: Colors.grey,
        ),
        body: Container(
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('assets/Zodiac/dash.jpg'),
                  fit: BoxFit.cover)),
          child: Column(
            // mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              //Edit Profile

              // a row with avator and container
              //container has a column that have two text widgets like =
              // We are going to have something like 0 =

              Column(
                children: [
                  Container(

                      //This container has both avatar and Details
                      padding: EdgeInsets.fromLTRB(20, 10, 20, 0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.fromLTRB(0, 19, 0, 0),
                            //an Avator next to another container
                            child: Padding(
                              padding: const EdgeInsets.only(left: 10.0),
                              child: Column(children: <Widget>[
                                const CircleAvatar(
                                  backgroundImage:
                                      AssetImage('assets/Zodiac/ava.jpg'),
                                  radius: 45.0,
                                ),
                                Container(
                                    child: RaisedButton(
                                  child: Text('Change Picture'),
                                  onPressed: (() {}),
                                )),
                              ]),
                            ),
                          ),
                          Divider(
                            height: 1,
                          ),
                          SizedBox(width: 38),
                          Container(
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: const <Text>[
                                  Text(
                                    'Username',
                                    style: TextStyle(
                                      fontSize: 39,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white,
                                    ),
                                  ),
                                  Text(
                                    'You are a Pisces',
                                    style: TextStyle(
                                      fontSize: 17,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white,
                                    ),
                                  ),
                                  Text(
                                    '8 March 1999',
                                    style: TextStyle(
                                      fontSize: 17,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white,
                                    ),
                                  ),
                                ]),
                          ),
                        ],
                      )),
                ],
              ),

              //Change profile picture

              Divider(
                height: 50.0,
                color: Colors.grey,
                indent: 40.0,
                endIndent: 40.0,
                thickness: 1.0,
              ),

              //Enter new Details
              SizedBox(
                height: 20,
              ),
              TextFClass(textDescript: 'Choose a username', obscureText: false),

              SizedBox(
                height: 12,
              ),
              TextFClass(
                textDescript: 'Create Password',
                obscureText: true,
              ),

              SizedBox(
                height: 12,
              ),
              TextFClass(
                textDescript: 'Confirm Password',
                obscureText: true,
              ),
              SizedBox(
                height: 12,
              ),
              TextFClass(textDescript: 'Confirm Now', obscureText: false),
              //Register Button
              SizedBox(
                height: 20,
              ),
              button(name: 'Update', route: DashBoard(), colorful: Colors.grey),
            ],
          ),
        ));
  }
}
